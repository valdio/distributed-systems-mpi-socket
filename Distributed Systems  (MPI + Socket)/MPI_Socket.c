#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h> 
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>


int main (argc, argv)
     int argc;
     char *argv[];
{

int rank, size; 
char file[50];
 char buffer[1024];
char process_winner = 0;//procesi qe gjeti file


/* Initialize MPI */
 if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
    	printf("MPI initialization failed!\n");
    exit(1);
    }

  /*
size mban munrin e proceseve ne te cilat do ekzekutohet programi 
rank mban id e procesit  0 - (size-1) 
*/
MPI_Comm_rank(MPI_COMM_WORLD, &rank);
MPI_Comm_size(MPI_COMM_WORLD, &size);

int end_search[size];

if (size < 2) {
    printf("Duhet te keni te pakten 2 procesore per te ekzekutuar kete program\n");
    MPI_Finalize();   /* Finalize nese eshte vetem 1 procesor */
    exit(0);
  }


if (rank == 0){
	/*procesi rrenje do mar emrin e FILE te kerkuar dhe duhet ti bej 
	broadcast ne te gjithe proceset paralele */
	printf("\nJepni emrin e FILE qe deshironi te kerkoni ne rrjet! \n");
	scanf("%s", file);
	printf("\n");

	}

	
	//Broadcast emrin e FILE  // broadcast eshte i dukshem per te gjithe proceset paralele
	MPI_Bcast(file, 50, MPI_CHAR, 0, MPI_COMM_WORLD);

	if(rank != 0){

	 printf( "\nProcesi %d nga %d eshte ne kerkim te file-it \"%s\"...\n\n", rank, size, file );
	}
    
	MPI_Barrier(MPI_COMM_WORLD);




if(rank == 0){

 MPI_Recv(&process_winner,1, MPI_BYTE, MPI_ANY_SOURCE,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

     printf("\n****************************************************\nFile u gjend ne procesin %d    \n****************************************************\n", process_winner);  
	printf("\nStarting socket transmition...\n\n\n");  

}

	
if(rank != 0){
//search for file
	 DIR *d;
	struct dirent *dir;
	char path[80];
	sprintf(path, "/home/valdio/Desktop/Network/%d",rank);
	d = opendir(path);
		
if(d){
		    while ((dir = readdir(d)) != NULL)
		    {
		        if( dir->d_name) 
			if (strcmp( dir->d_name, file) == 0){

		    	//FILE e kerkuar nga perdoruesi gjendet
			//Dergojm ID e procesit qe gjeti file

			process_winner= (char) rank;
			MPI_Send(&process_winner, 1, MPI_BYTE, 0,  0, MPI_COMM_WORLD);
			
}
		    }
  
		    closedir(d);
		}

	}


	int client_rank = (int)process_winner;
	MPI_Barrier(MPI_COMM_WORLD);

/************************************************************************************************/

	if(rank==0){

		//krijojme procesin Server
	int Socket, newSocket;

	  struct sockaddr_in serverAddr;
	  struct sockaddr_storage serverStorage;
	  socklen_t addr_size;


	  /*---- Krijimi i Socket. Socket permban 3 argumenta ----*/
	  /* 1) Internet domain 2) Stream socket 3) Default protocol (TCP ne kete rast) */
	  Socket = socket(PF_INET, SOCK_STREAM, 0);

	  
	  /*---- konfigurimi i struktures se adreses te serverit ----*/

	  /* Address family = Internet */
	  serverAddr.sin_family = AF_INET;
	  /* Vendosja e port number */
	  serverAddr.sin_port = htons(7891);
	  /* Vendosja e  IP ne localhost*/
	  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	 
	  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  



	  /*---- Bind  address structure  me  socket  ----*/
	  bind(Socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));


	  /*---- Vendoset severi ne pritje listen() ----*/
	  if(listen(Socket,5)==0)
	    printf("Serveri eshte ne pritje...\n\n");
	  else
	    printf("Error\n");
	


	  /*---- Pranimi i socket ne connection ----*/
	  addr_size = sizeof serverStorage;
	  newSocket = accept(Socket, (struct sockaddr *) &serverStorage, &addr_size);



	  /*---- Leximi imesazhit te klientit ne buffer ----*/
	
		recv(newSocket, buffer, 1024, 0);
	  


	//shkrimi ne file file   

		FILE *f = fopen("/home/valdio/Desktop/Network/server", "w");

	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}


	fprintf(f, "%s", buffer);


	fclose(f);


	printf("\n\nTransferimi i te dhenave perfundoj.\nTe dhenat gjenden ne \"Desktop/Network/server\"\n\n");
		}




/***********************************************************************************************/




/***********************************************************************************************/

			//krijojme procesin Client

	if(client_rank != 0 && rank == client_rank ){

	char C_buffer[1024];

	int clientSocket;
	 
	  struct sockaddr_in serverAddr;
	  socklen_t addr_size;

	  /*---- Krijimi i Socket. Socket permban 3 argumenta ----*/
	  /* 1) Internet domain 2) Stream socket 3) Default protocol (TCP ne kete rast) */
	  clientSocket = socket(PF_INET, SOCK_STREAM, 0);
	  
	 
	 /*---- konfigurimi i struktures se adreses se klientit ----*/

	 /* Address family = Internet */
	  serverAddr.sin_family = AF_INET; 
	 /* Vendosja e port number */
	  serverAddr.sin_port = htons(7891);
	 /* Vendosja e  IP ne localhost*/
	  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	  
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

	printf("Klienti %d eshte gati per transferim...\n\n", rank );
	  /*---- Lidhim socket me serverin duke perur address struct ----*/
	  addr_size = sizeof serverAddr;
	  connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);


	//lexo te dhenat nga file
		char file_path[80];
		sprintf(file_path, "/home/valdio/Desktop/Network/%d/%s",rank,file);


	char ch;
	int possition=0;
	FILE * r_f;
	r_f = fopen( file_path , "r");
	if (r_f) { 

	while (( ch = fgetc(r_f) )!= EOF)
	{
	C_buffer[possition]= (char)ch;
	possition++;
	}

	while(possition<1024){

	C_buffer[possition]='\0';
	possition++;

	}
	/*---- Dergimi i te dhenave te lexuara ne server ----*/	
	  send(clientSocket,C_buffer,1024,0);

	fclose(r_f);
	
	 }
	   

	}
	
	


	MPI_Finalize();
	return 0;
	}



