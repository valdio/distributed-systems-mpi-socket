#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h> 
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>


int main (argc, argv)
     int argc;
     char *argv[];
{

int rank, size; 
char file[50];
 char buffer[1024];
char process_winner = 0;//procesi qe gjeti file



 if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
    	printf("MPI initialization failed!\n");
    exit(1);
    }

MPI_Comm_rank(MPI_COMM_WORLD, &rank);
MPI_Comm_size(MPI_COMM_WORLD, &size);

int end_search[size];


if (rank == 0){
	printf("\nJepni FILE \n");
	scanf("%s", file);
	printf("\n");

	}

	MPI_Bcast(file, 50, MPI_CHAR, 0, MPI_COMM_WORLD);





if(rank == 0){

 MPI_Recv(&process_winner,1, MPI_BYTE, MPI_ANY_SOURCE,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);



}

	
if(rank != 0){
	 DIR *d;
	struct dirent *dir;
	char path[80];
	sprintf(path, "/home/valdio/Desktop/Network/%d",rank);
	d = opendir(path);
		
if(d){
		    while ((dir = readdir(d)) != NULL)
		    {
		        if( dir->d_name) 
			if (strcmp( dir->d_name, file) == 0){

			process_winner= (char) rank;
			MPI_Send(&process_winner, 1, MPI_BYTE, 0,  0, MPI_COMM_WORLD);
			
}
		    }
  
		    closedir(d);
		}

	}


	int client_rank = (int)process_winner;
	MPI_Barrier(MPI_COMM_WORLD);

/************************************************************************************************/

	if(rank==0){

		//krijojme procesin Server
	int Socket, newSocket;

	  struct sockaddr_in serverAddr;
	  struct sockaddr_storage serverStorage;
	  socklen_t addr_size;


	  Socket = socket(PF_INET, SOCK_STREAM, 0);


	  serverAddr.sin_family = AF_INET;
	  serverAddr.sin_port = htons(7891);
	  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	 
	  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  



	  bind(Socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

listen(Socket,5);


	  addr_size = sizeof serverStorage;
	  newSocket = accept(Socket, (struct sockaddr *) &serverStorage, &addr_size);



		recv(newSocket, buffer, 1024, 0);
	  



		FILE *f = fopen("/home/valdio/Desktop/Network/server", "w");

	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}


	fprintf(f, "%s", buffer);


	fclose(f);


		}



			//krijojme procesin Client

	if(client_rank != 0 && rank == client_rank ){

	char C_buffer[1024];

	int clientSocket;
	 
	  struct sockaddr_in serverAddr;
	  socklen_t addr_size;

 clientSocket = socket(PF_INET, SOCK_STREAM, 0);
	 
	  serverAddr.sin_family = AF_INET; 
	  serverAddr.sin_port = htons(7891);
	 serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	  
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  
addr_size = sizeof serverAddr;
	  connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);


		char file_path[80];
		sprintf(file_path, "/home/valdio/Desktop/Network/%d/%s",rank,file);


	char ch;
	int possition=0;
	FILE * r_f;
	r_f = fopen( file_path , "r");
	if (r_f) { 

	while (( ch = fgetc(r_f) )!= EOF)
	{
	C_buffer[possition]= (char)ch;
	possition++;
	}

	  send(clientSocket,C_buffer,1024,0);

	fclose(r_f);
	
	 }
	   

	}
	
	


	MPI_Finalize();
	return 0;
	}




